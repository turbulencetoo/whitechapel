#full_graph_maker.py
import json

def parse_raw_graph():
    white_to_black = {}
    black_to_both = {}

    with open("graph.txt") as rfp:
        rdata = rfp.readlines()

    for line in rdata:
        line = line.strip()
        if not line:
            continue
        line = line.split()

        if int(line[0]) < 1000:
            dict_to_use = white_to_black
        else:
            dict_to_use = black_to_both
        dict_to_use[line[0]] = line[1:]


    for bnode in black_to_both:
        for wnode in white_to_black:
            if bnode in white_to_black[wnode]:
                # wnode is adjacent to this black node
                black_to_both[bnode].append(wnode)

    blocks = []
    with open("blocks.txt") as rfp:
        for line in rfp:
            line = line.strip()
            line = line.split()
            blocks.append(line)

    master_json = {}

    master_json["Blacks"] = black_to_both
    master_json["Whites"] = white_to_black
    master_json["Blocks"] = blocks

    return master_json


def find_white_neighbors(wnode, master_json):
    # Map of nodes that wnode reaches to a list of paths that work
    # e.g. for node 119 as wnode  {121: [ [1109, 1161], [1173], [1173, 1176, 1174] ],...}
    white_neighbors = dict()
    path = [wnode]
    for bnode in master_json["Whites"][wnode]:
        find_white_neighbors_rec(bnode, path, white_neighbors, master_json)
    return white_neighbors

def find_white_neighbors_rec(cur_node, path, white_neighbors, master_json):
    if cur_node in master_json["Whites"]:
        if cur_node in white_neighbors:
            white_neighbors[cur_node].append(path[1:])
        else:
            white_neighbors[cur_node] = [path[1:]]
        return

    path.append(cur_node)
    for next_node in master_json["Blacks"][cur_node]:
        if next_node not in path:
            find_white_neighbors_rec(next_node, path, white_neighbors, master_json)
    path.pop()


def main():
    master_json = parse_raw_graph()

    master_json["WhiteAdj"] = {}
    for wnode in master_json["Whites"]:
        adj_paths = find_white_neighbors(wnode, master_json)
        master_json["WhiteAdj"][wnode] = adj_paths

    with open("full_graph.json", "w") as wfp:
        json.dump(master_json, wfp, sort_keys=True, indent=4)

if __name__ == '__main__':
    main()

# {   
#     "Whites": {
#         "1": [
#             "1001",
#             "1008"
#         ],
#         "10": [
#             "1021",
#             "1022"
#         ],
#         ...
#     }
#     "WhiteAdj": {
#         "100": {
#             "120": [
#                 [
#                     "1110"
#                 ]
#             ],
#             "122": [
#                 [
#                     "1149"
#                 ]
#             ],
#             "123": [
#                 [
#                     "1149",
#                     "1150",
#                     "1163"
#                 ],
#                 [
#                     "1149",
#                     "1163"
#                 ]
#             ],
#             ...
#         ...
#     }
#     "Blocks": [
#         [
#             "6",
#             "24"
#         ],
#         [
#             "7",
#             "1",
#             "26"
#         ],
#         ...
#     }
#     "Blacks": {
#         "1001": [
#             "1002",
#             "1",
#             "26"
#         ],
#         "1002": [
#             "1001",
#             "1009",
#             "2"
#         ],
#         ...
#     }
# }
