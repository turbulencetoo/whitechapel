#!/bin/sh

BIN=./jack_track.py
NAME=$1

rm "$NAME"*
$BIN $NAME init

$BIN $NAME jackstart 158
$BIN $NAME print
$BIN $NAME jack walk
$BIN $NAME print
$BIN $NAME detmiss 185 172
$BIN $NAME print
$BIN $NAME detloc 1210 1204 1165 1152 1166
$BIN $NAME print
$BIN $NAME jack walk
$BIN $NAME print
$BIN $NAME dethit 171
$BIN $NAME print
$BIN $NAME debug

# $BIN $NAME jackstart 147
# $BIN $NAME detloc 1160
# $BIN $NAME jack walk
# $BIN $NAME print
# $BIN $NAME debug
# $BIN $NAME detloc 1171
# $BIN $NAME jack walk
# $BIN $NAME print
# $BIN $NAME debug
# $BIN $NAME dethit 133
# $BIN $NAME print

# $BIN $NAME jackstart 147
# $BIN $NAME jack car
# $BIN $NAME print
# $BIN $NAME debug
# $BIN $NAME dethit 134
# $BIN $NAME print

# $BIN $NAME jackstart 57
# $BIN $NAME jack alley
# $BIN $NAME print
# $BIN $NAME jackstart 58
# $BIN $NAME print
# $BIN $NAME