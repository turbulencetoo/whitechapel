import re
import json

def find_white_neighbors(wnode, master_json):
    # Map of nodes that wnode reaches to a list of paths that work
    # e.g. for node 119 as wnode  {121: [ [1109, 1161], [1173], [1173, 1176, 1174] ],...}
    white_neighbors = dict()
    path = [wnode]
    for bnode in master_json["Whites"][wnode]:
        find_white_neighbors_rec(bnode, path, white_neighbors, master_json)
    return white_neighbors

def find_white_neighbors_rec(cur_node, path, white_neighbors, master_json):
    if cur_node in master_json["Whites"]:
        if cur_node in white_neighbors:
            white_neighbors[cur_node].append(path[:])
        else:
            white_neighbors[cur_node] = [path[:]]
        return

    path.append(cur_node)
    for next_node in master_json["Blacks"][cur_node]:
        if next_node not in path:
            find_white_neighbors_rec(next_node, path, white_neighbors, master_json)
    path.pop()


adj_mat = {}
with open("spots.txt") as infile:
    for line in infile:
        line = line.strip()
        match = re.match(r"spot = ([0-9]*).*connected = {(.*)}", line)
        grps = match.groups()
        node = grps[0]
        items = grps[1].split(", ")
        adj_mat[node] = items


with open("full_graph.json") as rfp:
    master_json = json.load(rfp)


for wnode in master_json["Whites"]:
    my_adjs = find_white_neighbors(wnode, master_json)
    his_adjs = set(adj_mat[wnode])
    if my_adjs != his_adjs:
        print("BAD NODE: {}".format(wnode))
        print("MINE: {} HIS: {}...\n diff1: {},  diff2: {}".format(my_adjs, his_adjs, my_adjs - his_adjs, his_adjs- my_adjs))
        print()
