#!/usr/bin/env python3
import sys
import os
import json
import pickle
import argparse

PATHS_SUFFIX = "_paths.pickle"
MASTER_JSON = "full_graph.json"

WALK = 'walk'
ALLEY = 'alley'
CAR = 'car'

class Node:
    '''
    Node represents one possible white circle where jack could be on a given turn.
    '''
    def __init__(self, idx):
        self.idx = idx  # str
        self.children = []
        self.parents = []

    def __str__(self):
        return repr(self)
    def __repr__(self):
        return "N({}, {}, p{}, c{})".format(self.idx, hexid(self), [(p.idx, hexid(p))for p in self.parents], [(c.idx, hexid(self)) for c in self.children])

def hexid(n):
    '''return last 4 digits of id in hex'''
    return "0x"+(hex(id(n))[-4:])

class JackTree:
    '''
    Represents a tree of all possible paths jack could have taken.
    Coalesce nodes at any given level so that levels try not to contain duplicate nodes.
    Duplicate nodes will be nescessitated in some first-move-of-carriage situations
    Also store the current detective locations so we know which moves are vaild
    '''
    def __init__(self, start_node_idxs):
        '''start_node_idxs -  a list of start nodes for the given night'''
        roots = [Node(idx) for idx in start_node_idxs]
        self.turns = {"0": roots}
        self.cur_det_locs = []
        self.cur_level = "0"

    def add_node(self, idx, parent, merge=True):
        ''' Add a node to the tree. If its already in the tree, and we're merging...
        TODO: nodes in sets based on hash!'''
        cur_nodes = self.turns[self.cur_level]
        if merge:

            # if we can find an identical node in this row, use it as new_node instead
            new_node = next((node for node in cur_nodes if node.idx == idx), None)
            if not new_node:
                # couldn't find it
                new_node = Node(idx)
                cur_nodes.append(new_node)
        else:
            # not merging
            new_node = Node(idx)
            cur_nodes.append(new_node)

        # in all cases, set parent and child pointers, and add this to the list of nodes from this turn
        parent.children.append(new_node)
        new_node.parents.append(parent)

        # I think that the carriage function will want a ponter to the node
        # so they can do move 2 of the carriage
        return new_node

    def next_level(self):
        self.cur_level = str(int(self.cur_level) + 1)
        self.turns[self.cur_level] = []

    def debug(self):
        for turn, nodes in sorted(self.turns.items()):
            print(turn, nodes)

def find_node(cur_node, parent_node, idx_to_find):
    '''
    recursive function that will find a node in our tree,
    severing any paths that don't have the node in them by a certain time.
    '''
    if cur_node.idx == idx_to_find:
        return True
    if not cur_node.children:
        # made it to the bottom without finding
        sever_relationship(parent_node, cur_node)
        return False

    results = []
    for child in cur_node.children[:]:
        results.append(find_node(child, cur_node, idx_to_find))

    if not any(results):
        sever_relationship(parent_node, cur_node)
        return False
    else:
        # at least one child with live path
        return True


def dont_find_node(tree, idx_to_not_find):
    '''
    ensure a node is not node in our tree,
    severing any paths that have the node in them before a certain time.
    '''
    for turn, nodes in sorted(tree.turns.items()):
        for node in nodes:
            if node.idx == idx_to_not_find:
                # sever all parents so it will get cleaned by street sweeper
                for parent in node.parents[:]:
                    sever_relationship(parent, node)


def dont_find_node_missed_arrest(tree, idx_to_not_find):
    '''ensure that a specific node is not at a specific level'''

    for level, nodes in sorted(tree.turns.items()):
        # iterate to leaves of the tree
        pass

    for node in nodes:
        if node.idx == idx_to_not_find:
            # sever all parents so it will get cleaned by street sweeper
            for parent in node.parents[:]:
                sever_relationship(parent, node)


def street_sweeper(tree):
    '''
    for each row, removes all nodes with no parents by severing ties with their children
    '''
    for turn, nodes in sorted(tree.turns.items()):
        if turn == "0":
            # dont prune roots (technically a root with no children can be pruned)
            continue
        for node in nodes:
            if not node.parents:
                for child in node.children[:]:  # need a deep copy to avoid problems
                    sever_relationship(node, child)

        # remake the row list with only the node we're keeping
        tree.turns[turn] = [n for n in nodes if n.parents]


def sever_relationship(parent, child):
    # print("severing relationship:", parent, child)
    parent.children.remove(child)
    child.parents.remove(parent)


def init(game_name):
    '''start a new game by initializing the underlying file'''

    if os.path.exists(game_name + PATHS_SUFFIX):
        print("A game with name {} already exists".format(game_name))
        raise OSError

    # Maps nights 1-4 to a JackTree for that night
    paths_dict = {"CurrentTree": None,
                  "AllNights": []}

    with open(game_name + PATHS_SUFFIX, "wb") as wfp:
        pickle.dump(paths_dict, wfp)

    print("Created a game named", game_name)

def detloc(full_map, paths, args):
    '''update the current detective locations'''

    print("Detectives now at", args)
    # sanitize to indices 1000+
    for i in range(len(args)):
        if int(args[i]) < 1000:
            args[i] = str(int(args[i])+1000)

    tree = paths["CurrentTree"]
    tree.cur_det_locs = args


def detmiss(full_map, paths, args):
    '''prune the tree based on which nodes were sniffed and missed'''

    print("No scent of jack found at", args)
    tree = paths["CurrentTree"]
    for node_idx in args:
        dont_find_node(tree, node_idx)
    street_sweeper(tree)


def detarrest(full_map, paths, args):
    '''record which nodes were arrested and missed'''

    print("Missed an arrest on jack at", args)
    tree = paths["CurrentTree"]
    for node_idx in args:
        dont_find_node_missed_arrest(tree, node_idx)
    street_sweeper(tree)


def dethit(full_map, paths, args):
    '''record which nodes were sniffed and hit'''

    print("Found scent of jack at", args)
    tree = paths["CurrentTree"]
    for root in tree.turns["0"]:
        for node_idx in args:
            find_node(root, None, node_idx)
    street_sweeper(tree)


def jack(full_map, paths, args):
    '''jack makes a move'''

    print("Jack has moved by {}".format(args[0]))
    move = args[0]
    tree = paths["CurrentTree"]
    prev_level = tree.cur_level
    tree.next_level()

    if move == WALK:
        for prev_node in tree.turns[prev_level]:
            for next_node_idx, paths_list in full_map["WhiteAdj"][prev_node.idx].items():
                if any(all(det_node_idx not in tree.cur_det_locs for det_node_idx in path) for path in paths_list):
                    # valid path exists
                    tree.add_node(next_node_idx, prev_node)

    elif move == ALLEY:
        for prev_node in tree.turns[prev_level]:
            # grab all nodes that share a block with this previous node, excluding this previous node itself
            for block in full_map["Blocks"]:
                if prev_node.idx in block:
                    for next_node_idx in block:
                        if next_node_idx != prev_node.idx:
                            tree.add_node(next_node_idx, prev_node)

    elif move == CAR:
        for prev_node in tree.turns[prev_level]:
            for next_node_idx, paths_list in full_map["WhiteAdj"][prev_node.idx].items():
                # dont worry about valid pathfor carriage
                tree.add_node(next_node_idx, prev_node, merge=False)
        
        # Now do move 2 of carriage
        prev_level = tree.cur_level
        tree.next_level()
        for prev_node in tree.turns[prev_level]:
            for next_node_idx, paths_list in full_map["WhiteAdj"][prev_node.idx].items():
                # make sure we dont return form whence we came
                assert len(prev_node.parents) == 1
                if next_node_idx != prev_node.parents[0].idx:
                    tree.add_node(next_node_idx, prev_node)
    else:
        raise ValueError


def jackstart(full_map, paths, args):
    '''jack's starting location'''

    print("Murder at {}!!".format(args))
    new_graph = JackTree(args)
    paths["CurrentTree"] = new_graph
    paths["AllNights"].append(new_graph)


def print_locs(full_map, paths, args):

    tree = paths["CurrentTree"]
    print("Jack is at:", sorted(node.idx for node in tree.turns[tree.cur_level]))


def debug(full_map, paths, args):

    tree = paths["CurrentTree"]
    tree.debug()

def main():
    usage ='''usage:
{} <game_name> cmd [args]

game_name            a unique id to give to this game
cmds:
 init                creates a new game
 detloc [5 locs]     updates the detective's positions
 detmiss [n locs]    indicates locations that were sniff misses
 detarrest [n locs]  indicates locations that were arrest misses
 dethit [n locs]     indicates locations that were sniff hits
 jack <move>         records jack's move
 jackstart [1-2 loc] indicates the murder site(s)
 print               print out all possible current locations for jack

<move> options:
 {}, {}, {}
'''.format(sys.argv[0], WALK, ALLEY, CAR)

    parser = argparse.ArgumentParser(description='Premium Jack Tracking Software')
    parser.add_argument('-c', type=str, dest='cmd', required=True, help='The command to run')
    parser.add_argument('-n', type=str, dest='name', required=True, help='The unique game name')
    parser.add_argument('-m', type=str, dest='map', default=MASTER_JSON)
    parser.add_argument('args', nargs='*')
    parsed = parser.parse_args()

    cmd_map = {"init": init,
               "detloc": detloc,
               "detmiss": detmiss,
               "detarrest": detarrest,
               "dethit": dethit,
               "jack": jack,
               "jackstart": jackstart,
               "print": print_locs,
               "debug": debug,
               }

    game_name = parsed.name
    cmd = parsed.cmd
    args = parsed.args
    map_file = parsed.map

    if cmd not in cmd_map:
        print(usage)
        sys.exit(2)

    if cmd == "init":
        ret = cmd_map[cmd](game_name)
    else:
        with open(game_name + PATHS_SUFFIX, "rb") as pfile,\
             open(map_file) as mfile:
            paths = pickle.load(pfile)
            full_map = json.load(mfile)
        ret = cmd_map[cmd](full_map, paths, args)

        with open(game_name + PATHS_SUFFIX, "wb") as pfile:
            pickle.dump(paths, pfile)

    sys.exit(ret)


if __name__ == '__main__':
    main()